//import java.awt.*;
//import java.lang.reflect.Array;
import java.util.Arrays;
//import java.util.ArrayList;

public class ColorSort {
   /**
    *
    * Lisatud Erle Maido kommentaarid
    *
    * Esialgset varianti ei ole tehniliste viperuste tõttu bitbucketis, on võimalik Moodlest siia laadida.
    *
    *
    *
    * testimiseks
    * @param param sorteeritav list
    */
   public static void main(String[] param) {
      Color arr[] = new Color[]{Color.green, Color.green, Color.blue, Color.blue, Color.red,
              Color.green, Color.blue, Color.green, Color.green, Color.red, Color.blue, Color.green,
              Color.red, Color.red, Color.red};

      reorder(arr);
//        System.out.println(Arrays.toString(arr));

      arr = new Color[]{Color.green, Color.blue, Color.green, Color.green, Color.green,
              Color.green, Color.blue, Color.green, Color.red, Color.blue, Color.red,
              Color.red, Color.blue, Color.red, Color.blue, Color.red};
      reorder(arr);
//        System.out.println(Arrays.toString(arr));
   }

   /**
    * Loendab ära kõik kolm [] olevat värvi pallid, kirjutab üle originaali indeksit/
    * /pallide arvu loendades
    * @param balls
    */

   public static void reorder(Color[] balls) {


      // lihtne ja loogiline lahendus antud ülesande puhul, kuid kui värve juurde lisatakse,
      // siis tekib väga palju IF lauseid

      int redCount = 0;
      int greenCount = 0;
      int blueCount = 0;


      // tsükkel, mis loendab värve

      for (Color ball : balls) {
         if (ball == Color.red) {
            redCount++;
         } else if (ball == Color.green) {
            greenCount++;
         } else if (ball == Color.blue) {
            blueCount++;
         }
      }


      // tsükkel, mis hakkab järjest massiivi värve kirjutama seni maani,
      // kui palju neid oli vastavalt eelmises tsüklis leitud arvule
      for (int i = 0; i < balls.length; i++) {
         if (i < redCount) {
            balls[i] = Color.red;
         } else if (i < greenCount + redCount) {
            balls[i] = Color.green;
         } else if (i < blueCount + greenCount + redCount) {
            balls[i] = Color.blue;
         }
      }
   }

   enum Color {red, green, blue}
}

// küsimus õppejõule
//
//   public static void reorder(Color[] balls) {
//        ArrayList<Color> red = new ArrayList<>();
//        ArrayList<Color> green = new ArrayList<>();
//        ArrayList<Color> blue = new ArrayList<>();
//                               // kolm eraldi arraylisti ja 1 loop nende vastavatesse lisamiseks
//        for (Color ball : balls) {
//            if (Color.red == ball) {
//                red.add(ball);
//            } else if (Color.green == ball) {
//                green.add(ball);
//            } else if (Color.blue == ball) {
//                blue.add(ball);
//            }
//          }
//                      // lisan kõik kokku
//        red.addAll(green);
//        System.out.println(red);
//        red.addAll(blue);

//        red.toArray();
//                          // siin see toarray ei tööta, .getclass.getname returnib ikka arraylisti.
// milles asi siin olla võiks?
//        System.out.println(red.getClass().getName());
//
//   }
